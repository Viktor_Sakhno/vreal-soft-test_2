import {combineReducers} from 'redux';
import * as usersReducer from './users';

export interface IStoreState {
    users: usersReducer.IState
}

export const initState: IStoreState = {
    users: usersReducer.initState
}

const rootReducer = combineReducers<IStoreState>({
    users: usersReducer.reducer
})

export default rootReducer;

export type AppState = ReturnType<typeof rootReducer>;

declare module 'react-redux' {
    export interface DefaultRootState extends AppState {
    }
}
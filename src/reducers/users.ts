import {IUser} from '../models';

export enum actionTypes {
    GET = 'USERS_GET_SUCCESS',
    FAILURE = 'USERS_GET_FAILURE',
}

interface IUsersActionGet {
    type: actionTypes.GET
    payload: IUser[]
}

interface IUsersActionFailure {
    type: actionTypes.FAILURE
}

type UsersActions = IUsersActionGet | IUsersActionFailure;

export interface IState {
    readonly users: IUser[]
}

export const initState: IState = {
    users: [],
}

export function reducer(state = initState, action: UsersActions): IState {
    switch (action.type) {
        case actionTypes.GET:
            return {
                users: action.payload,
            }
        case actionTypes.FAILURE:
            return initState
        default:
            return state
    }
}

import React, {FC} from 'react';
import {Provider} from 'react-redux';
import {Router, Switch, Route} from 'react-router'
import {Public} from '../Public';

import store from '../../store/store';
import history from '../../utils/history';


const App: FC = () => {
    return (
        <Provider store={store}>
            <Router history={history}>
                <Switch>
                    <Route path="/" component={Public}/>
                </Switch>
            </Router>
        </Provider>
    );
}

export default App;
import React, {FC} from 'react'
import {NavLink} from 'react-router-dom';
import {Card, Avatar} from 'antd'

import {IUser} from 'models/index';

import styles from './styles.module.scss'

type Props = {
    user: IUser;
}

export const UserCard: FC<Props> = ({user}) => {
    return (
        <NavLink className={styles.link} to={`/user/${encodeURIComponent(user.profileUrl)}`}
                 style={{textDecoration: 'none'}}>
            <Card
                key={user.profileUrl}
                className={styles.card}
                hoverable
                cover={user.avatarUrl
                    ? <div className={styles.avatarBox}>
                        <Avatar
                            className={styles.avatar}
                            size={180}
                            src={user.avatarUrl}
                        />
                    </div>
                    : <div className={styles.noImage}>No image</div>
                }
            >
                <Card.Meta className={styles.cardMeta} title={user.name} description="LinkedIn"/>
            </Card>
        </NavLink>
    )
}


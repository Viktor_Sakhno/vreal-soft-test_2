import React, {FC} from 'react';
import {useSelector} from 'react-redux';
import {UserCard} from './UserCard';
import {Empty} from 'antd';


export const UsersList: FC = () => {
    const users = useSelector(state => state.users.users);
    return (
        <>
            {users.length
                ? users.map(user => <UserCard key={user.profileUrl} user={user}/>)
                : <Empty description="There are no users with this name"/>
            }
        </>
    );
}
import React, {FC, useState} from 'react';
import {SearchInput} from './SearchInput';
import {AppLoading} from '../../../components/AppLoading';
import {notification} from 'antd';
import {WarningFilled} from '@ant-design/icons'

import * as usersActions from 'actions/users'


export const SearchUsers: FC = () => {
    const [searchValue, setSearchValue] = useState<string>('');
    const [isLoading, setIsLoading] = useState<boolean>(false);

    const loadUsers = async () => {
        setIsLoading(true);
        try {
            if (!searchValue) return;
            await usersActions.getUsers(searchValue);
        } catch (e) {
            notification.open({
                message: 'Fetch users list error',
                description: e.message || e,
                icon: <WarningFilled style={{color: 'red'}}/>,
            })
        } finally {
            setIsLoading(false);
            setSearchValue('');
        }
    }

    return (
        <>
            {
                isLoading ?
                    <AppLoading loaderValue="Loading users..."/>
                    :
                    <SearchInput
                        searchValue={searchValue}
                        setSearchValue={setSearchValue}
                        placeholder="Search by user name"
                        onSearch={loadUsers}
                    />
            }
        </>
    )
}

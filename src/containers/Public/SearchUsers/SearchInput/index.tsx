import React, {FC} from 'react';
import {Col, Row, Input} from 'antd';

import styles from './styles.module.scss';


type Props = {
    searchValue: string;
    placeholder: string;
    onSearch: (value: string) => void;
    setSearchValue: (e: string) => void;
};

export const SearchInput: FC<Props> = ({onSearch, searchValue, setSearchValue, placeholder}) => {

    return (
        <Row justify="center">
            <Col className={styles.col}>
                <Input.Search
                    placeholder={placeholder}
                    size="large"
                    enterButton="Search"
                    onSearch={onSearch}
                    value={searchValue}
                    onChange={e => setSearchValue(e.target.value)}
                />
            </Col>
        </Row>
    );
}

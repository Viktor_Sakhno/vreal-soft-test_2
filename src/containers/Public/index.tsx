import React, {FC} from "react";
import {Route, Switch, Redirect} from "react-router-dom";
import {User} from "./User";
import {UsersList} from "./UsersList";
import {SearchUsers} from "./SearchUsers";
import {Header} from "../../components/Header";
import {Footer} from "../../components/Footer";
import {Layout} from "antd";

import styles from "./styles.module.scss";

export const Public: FC = () => {
    return (
        <Layout className={styles.layout}>
            <Header/>
            <Layout.Content className={styles.content}>
                <Switch>
                    <Route exact path="/users-list" component={UsersList}/>
                    <Route
                        exact
                        path="/user/:id"
                        children={<User/>}
                    />
                    <Route
                        exact
                        path="/search-users"
                        children={<SearchUsers/>}
                    />
                    <Redirect exact from="/" to="/search-users"/>
                </Switch>
            </Layout.Content>
            <Footer/>
        </Layout>
    );
};

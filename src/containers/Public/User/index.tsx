import React, {FC, useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import {AppLoading} from "../../../components/AppLoading";
import {Avatar, Divider, notification} from "antd";
import {LinkedinFilled, WarningFilled} from "@ant-design/icons";

import history from "../../../utils/history";
import {IUserFull} from "../../../models";
import {getUser} from "../../../actions/user";

import styles from "./styles.module.scss";


export const User: FC = () => {
    const [user, setUserData] = useState<IUserFull>();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const {id} = useParams<{ id: string }>();

    useEffect(() => {
        loadUserData();
    }, []);

    const loadUserData = async () => {
        setIsLoading(true);
        try {
            const response = await getUser(id);
            setUserData(response);
        } catch (e) {
            notification.open({
                message: "Fetch user data error",
                description: e.message || e,
                icon: <WarningFilled style={{color: "red"}}/>,
            });
            history.push("/usersList");
        } finally {
            setIsLoading(false);
        }
    };

    const customDivider = (arr: any[] | undefined, idx: number) => {
        if (!arr) return null;
        return arr.length > 0 && idx !== arr.length - 1 ? (
            <Divider style={{margin: "10px 0"}}/>
        ) : null;
    };

    return (
        <>
            {isLoading ? (
                <AppLoading loaderValue="Loading user..."/>
            ) : (
                <div className={styles.card}>
                    <div className={styles.headerFon}>''</div>
                    <LinkedinFilled className={styles.linkedIn}/>
                    <div className={styles.cardHeader}>
                        <Avatar
                            size={230}
                            src={user?.profile?.imageurl}
                            className={styles.avatar}
                        />
                        <div className={styles.profile}>
                            <h1 className={styles.profileName}>{user?.profile?.name}</h1>
                            <h2 className={styles.profileHeadline}>
                                {user?.profile?.headline}
                            </h2>
                            <div className={styles.profileLocation}>
                                {user?.profile?.location}
                            </div>
                        </div>
                    </div>
                    <div className={styles.cardBody}>
                        {!!user?.about?.text && (
                            <div className={styles.cardSection}>
                                <h3 className={styles.cardSectionTitle}>About</h3>
                                {user.about.text}
                            </div>
                        )}
                        {!!user?.positions?.length && (
                            <div className={styles.cardSection}>
                                <h3 className={styles.cardSectionTitle}>Experience</h3>
                                {user.positions.map((item, idx) => (
                                    <div key={idx} className={styles.position}>
                                        <div className={styles.positionTitle}>{item.title}</div>
                                        <a
                                            className={styles.title}
                                            href={item.link}
                                            target="_blank"
                                            rel="noreferrer"
                                        >
                                            {item.companyName}
                                        </a>
                                        <div className={styles.positionDate}>
                                            {item.date1} {item.date2}
                                        </div>
                                        <div className={styles.positionLocation}>
                                            {item.location}
                                        </div>
                                        {customDivider(user.positions, idx)}
                                    </div>
                                ))}
                            </div>
                        )}
                        {!!user?.educations?.length && (
                            <div className={styles.cardSection}>
                                <h3 className={styles.cardSectionTitle}>Education</h3>
                                {user.educations.map((item, idx) => (
                                    <div key={idx} className={styles.education}>
                                        <div className={styles.title}>{item.title}</div>
                                        <div className={styles.educationDegree}>
                                            {`${item.degree}, ${item.fieldOfStudy}`}
                                        </div>
                                        <div
                                            className={styles.educationDate}
                                        >{`${item.date1} - ${item.date2}`}</div>
                                        {customDivider(user.educations, idx)}
                                    </div>
                                ))}
                            </div>
                        )}
                        {!!user?.skills?.length && (
                            <div className={styles.cardSection}>
                                <h3 className={styles.cardSectionTitle}>Skills & endorsements</h3>
                                {user.skills.map((item, idx) => (
                                    <div key={idx} className={styles.skill}>
                                        <div className={styles.title}>{item.title}</div>
                                        <div className={styles.skillLevel}>
                                            {item.count ? `• ${item.count}` : ""}
                                        </div>
                                    </div>
                                ))}
                            </div>
                        )}
                        {!!user?.certifications?.length && (
                            <div className={styles.cardSection}>
                                <h3 className={styles.cardSectionTitle}>
                                    Licenses & certifications
                                </h3>
                                {user.certifications.map((item, idx) => (
                                    <div key={idx} className={styles.course}>
                                        <div className={styles.title}>{item}</div>
                                    </div>
                                ))}
                            </div>
                        )}
                        {!!user?.accomplishments?.length && (
                            <>
                                {user.accomplishments.map((accomplishment, idx) => (
                                    <div key={idx} className={styles.cardSection}>
                                        <h3 className={styles.cardSectionTitle}>
                                            {accomplishment.title}
                                        </h3>
                                        {accomplishment?.items?.length &&
                                        accomplishment?.items?.map((item, idx) => (
                                            <div key={idx} className={styles.language}>
                                                <div className={styles.title}>{item}</div>
                                            </div>
                                        ))}
                                    </div>
                                ))}
                            </>
                        )}
                    </div>
                </div>
            )}
        </>
    );
};

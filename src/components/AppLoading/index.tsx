import React, {FC} from 'react'
import {Col, Row, Spin} from 'antd';

import styles from './styles.module.scss'


type Props = {
    loaderValue: string;
}

export const AppLoading: FC<Props> = ({loaderValue}) => (
    <Row justify="center">
        <Col>
            <div className={styles.loader}>
                <Spin size="large" tip={loaderValue}/>
            </div>
        </Col>
    </Row>
)


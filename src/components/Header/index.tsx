import React, {FC, useEffect} from 'react';
import {useLocation} from 'react-router';
import {useDispatch, useSelector} from 'react-redux';
import {Menu, Layout} from 'antd';

import history from '../../utils/history';
import {LocalDataUser} from '../../models';
import {actionTypes as usersActionTypes} from '../../reducers/users';

import styles from './styles.module.scss'


export const Header: FC = () => {
    const users = useSelector(state => state.users.users);

    const location = useLocation();
    const dispatch = useDispatch();

    useEffect(() => {
        const localData: LocalDataUser[] | null = JSON.parse(String(localStorage.getItem('loadedUsers')));
        const lastData = localData && localData[localData.length - 1];
        if (lastData) {
            dispatch({type: usersActionTypes.GET, payload: lastData.users});
        }
    }, [])

    const goToSearch = () => {
        history.push('/search-users');
    }
    const goToUsersList = () => {
        history.push('/users-list');
    }

    return (
        <Layout.Header>
            <Menu theme="dark" mode="horizontal" selectedKeys={[location.pathname]}>
                <Menu.Item className={styles.item} key="/search-users" onClick={goToSearch}>Search</Menu.Item>
                <Menu.Item className={styles.item} key="/users-list" onClick={goToUsersList} disabled={!users.length}>
                    List
                </Menu.Item>
            </Menu>
        </Layout.Header>
    );
}

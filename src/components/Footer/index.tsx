import React, { FC } from "react";
import { Layout } from "antd";

import styles from "./styles.module.scss";

export const Footer: FC = () => {
  return (
    <Layout.Footer className={styles.footer}>VReal Soft©2021</Layout.Footer>
  );
};

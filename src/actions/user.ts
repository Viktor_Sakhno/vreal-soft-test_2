import {api} from '../config';
import {IUserFull, LocalDataSingleUser} from '../models';


export async function getUser(url: string): Promise<IUserFull> {
    const localData: LocalDataSingleUser[] = JSON.parse(String(localStorage.getItem('loadedSingleUsers')));
    let filteredData: LocalDataSingleUser[] = [];
    let singleSearchUser: LocalDataSingleUser[] = [];

    if (localData) {
        filteredData = localData.filter(item => +new Date() - item.data < 3600000);
        singleSearchUser = filteredData.filter(item => item.url === url);
    }

    if (!singleSearchUser.length) {
        const res = await api.get(`linkedIn/${url}`);
        if (res.status !== 200) throw new Error(`Can't fetch user by url ${url}`);

        const localDataNew = JSON.stringify([...filteredData, {user: res.data, url, data: +new Date()}])
        localStorage.setItem('loadedSingleUsers', localDataNew);

        return res.data;

    } else {
        return singleSearchUser[0].user;
    }
}
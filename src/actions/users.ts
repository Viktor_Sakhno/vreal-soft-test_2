import {api} from '../config';
import store from 'store/store';
import history from '../utils/history';
import {actionTypes as usersActionTypes} from 'reducers/users';
import {LocalDataUser} from '../models';



export async function getUsers(value: string): Promise<void> {
    const localData: LocalDataUser[] | null = JSON.parse(String(localStorage.getItem('loadedUsers')));
    let expiresData: LocalDataUser[] = [];
    let singleSearchData: LocalDataUser[] = [];
    let filteredData : LocalDataUser[] = [];

    if (localData) {
        expiresData = localData.filter(item => +new Date() - item.data < 3600000);
        singleSearchData = expiresData.filter(item => item.value.toUpperCase() === value.toUpperCase());
        filteredData = expiresData.filter(item => item.value.toUpperCase() !== value.toUpperCase());
    }

    if (!singleSearchData.length) {
        const res = await api.get('linkedIn', {params: {userName: value}});
        if (res.status !== 200) throw new Error(`Request failed with status code ${res.request.status}`);

        const localDataNew = JSON.stringify([...expiresData, {users: res.data, value, data: +new Date()}])
        localStorage.setItem('loadedUsers', localDataNew);

        store.dispatch({type: usersActionTypes.GET, payload: res.data});
    } else {
        localStorage.setItem('loadedUsers', JSON.stringify([...filteredData, {users: singleSearchData[0].users, value, data: +new Date()}]));
        store.dispatch({type: usersActionTypes.GET, payload: singleSearchData[0].users});
    }

    history.push('/users-list');
}


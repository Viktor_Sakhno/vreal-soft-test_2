export interface IUser {
    profileUrl: string;
    name: string;
    avatarUrl: string;
}

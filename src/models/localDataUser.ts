import {IUser} from './user';

export type LocalDataUser = {
    users: IUser[],
    value: string,
    data: number
}
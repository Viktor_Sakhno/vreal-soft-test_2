export interface IUserFull {
  profile?: {
    name: string;
    headline: string;
    location: string;
    connections: string;
    imageurl: string;
    summary: string;
  };
  about?: {
    text: string;
  };
  positions?: [
    {
      title: string;
      link: string;
      url: string;
      companyName: string;
      location: string;
      date1: string;
      date2: string;
    }
  ];
  educations?: [
    {
      title: string;
      degree: string;
      url: string;
      fieldOfStudy: string;
      date1: string;
      date2: string;
    }
  ];
  skills?: [{ title: string; count?: string }];
  recommendations?: {
    givenCount: string;
    receivedCount: string;
    given: [{}];
    received: [];
  };
  accomplishments?: [{ count: string; title: string; items: string[] }];
  courses?: [{ name: string }];
  languages?: [
    {
      name: string;
      proficiency: string;
    }
  ];
  projects?: boolean | [];
  peopleAlsoViewed?: [];
  volunteerExperience?: [];
  contact?: [];
  certifications: string[];
}

import {IUserFull} from './userFull';

export type LocalDataSingleUser = {
    user: IUserFull,
    url: string,
    data: number
}